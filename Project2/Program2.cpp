/*
Name: 	Luke Simmons

Course : CS 201
Program : Program 2 Sportsball
Due Date : 2/15/2015
Description :The NSL needs a program to keep track of the scores, wins,games and average scores.
Inputs : Variable "input" will get the input file from the user.
Outputs : Variable "Output" will get the output file from the user.
Algorithm :
1. A class "Team" needs to be created in a header file.
2. Team needs a default constructor- accepts no parameters. A regular constructor- accepts team name.
3. team needs public functions that accomplish:
	get name
	get average score
	get score for a specific game- accepts an integer for the game number
	get number of games won
	set score of game- accepts an integer for the game score.
	increment wins
4. Team needs the following private variables.
	a variable to keep track of wins
	an array to keep scores- max length of 150
	a variable to set the team name.
5. A .cpp needs to be created to define "Team".
6. The user should be prompted for the input and output file,should check input file for availability.
7. Read the input file read the names of the teams into a array of type teams. length should be the number of teams.
7. get the scores and the winners of each game from the file and store them within the class.
8. output the best and worst team with the team name, number of games won and average score. Out put this to the file.
9. close the input file.

*/

#include <iostream>
# include <fstream>
# include <string>
#include "Team.h"
using namespace std;
int main()
{
	Team teams[16];// The teams will go into this array
	string addNew;
	int NoOfTeams = 0;
	int k;
	int bestTeam = 0;
	int worstTeam = 0;
	string input;// will  be the input file
	string output;// will be the output file
	cout << "Enter the input file ";
	cin >> input;// prompt the user for the input file
	cout << endl;

	cout << "Enter the output file ";
	cin >> output;// prompt the user for the output file
	cout << endl;
	ifstream fin(input);
	if (!fin.good())// check for the availabilty of the input file.
	{
		cout << "file not found" << endl;
		system("pause");
		return 0;
	}
	ofstream fout(output);
	fin >> NoOfTeams;// Reads the first line which is the number of teams
	for (k = 0; k < NoOfTeams; k++)
	{
		fin >> addNew;
		teams[k] = Team(addNew);
	}

	while (fin.good())// this large loop will have two main loops.The first one will if the fist score read in is bigger
		// then the second. The other loop is the opposite.
	{
		string temp;
		string temp2;
		int score;
		int score2;
		fin >> temp >> score >> temp2 >> score2;
		if (score > score2)// If score 1 is larger than score 2.
		{
			for (k = 0; k < NoOfTeams; k++)
			{
				if (teams[k].getName() == temp)// will loop through to find the correct team
				{	// will call the appropriate methods
					teams[k].incrementWin();
					teams[k].setScore(score);
					teams[k].incrementGamesPlayed();
				}
				if (teams[k].getName() == temp2)
				{
					teams[k].incrementGamesPlayed();
					teams[k].setScore(score2);
				}
			}
		}
		if (score < score2) // If score 2 is larger than score 1
		{
			for (k = 0; k < NoOfTeams; k++)
			{
				if (teams[k].getName() == temp2)// will loop through to find the correct team.
				{	// will call the appropriate methods
					teams[k].incrementWin();
					teams[k].setScore(score2);
					teams[k].incrementGamesPlayed();
				}
				if (teams[k].getName() == temp)
				{
					teams[k].incrementGamesPlayed();
					teams[k].setScore(score);

				}
			}
		}
	}
	// this will loop through and determine the winner
	for (k = 1; k < NoOfTeams; k++)
	if (teams[k].gamesWon() > teams[bestTeam].gamesWon())
		bestTeam = k;
	// this will loop through and determine the loser.
	for (k = 1; k < NoOfTeams; k++)
	if (teams[k].gamesWon() < teams[worstTeam].gamesWon())
		worstTeam = k;
	fin.close();// closes the file
	// outputs the data to the file.
	fout << "Best Team" << endl;
	fout << teams[bestTeam].getName() << endl;
	fout << "They won " << teams[bestTeam].gamesWon() << " games." << endl;
	fout << "With an average score of " << teams[bestTeam].getAverageScore()<< endl;
	fout << " " << endl;
	fout << "Worst Team" << endl;
	fout << teams[worstTeam].getName() << endl;
	fout << "They won " << teams[worstTeam].gamesWon() << " games." << endl;
	fout << "With an average score of " << teams[worstTeam].getAverageScore();
	fout << endl;

}