# include "Team.h"
# include <iostream>
using namespace std;

Team::Team()//constructor
{
	gamesPlayed = 0;
	Wins = 0;
	nameOfTeam = "NO Name Entered";
	i = 0;


}
Team::Team(string teamName)//constructor
{
	nameOfTeam = teamName;
	gamesPlayed = 0;
	Wins = 0;
	i = 0;
}
double Team::getAverageScore()// returns the average score
{
	double total = 0.0;
	int k;
	for (k = 1; k <= gamesPlayed; k++)
		total += scores[k];
	return (total/gamesPlayed);
}
int Team::getGameScore(int gameNumber)// returns the gamescore of a specific game- accepts game number games will start at 1 and not 0.
{
	return scores[gameNumber];
}
string Team::getName()// returns the Team Name
{
	return nameOfTeam;
}
int Team::gamesWon()// returns games won
{
	return Wins;
}
void Team::setScore(int score)// sets the score- accepts score
{
	i += 1;
	scores[i] = score;
	
}
void Team::incrementWin() // increments the win
{
	Wins += 1;
}
void Team::incrementGamesPlayed()// increment games played.
{
	gamesPlayed += 1;
}