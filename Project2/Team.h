# pragma once
#include <string>
using namespace std;

class Team
{
public:
	// comments in the cpp file.
	Team();
	Team(string teamName);
	string getName();
	double getAverageScore();
	int getGameScore(int gameNumber);
	int gamesWon();
	void setScore(int score);
	void incrementWin();
	void incrementGamesPlayed();
	
private:
	string nameOfTeam;
	int gamesPlayed;
	int Wins;
	int scores[150];
	int i;
};